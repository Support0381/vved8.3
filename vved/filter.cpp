#include "filter.h"
#include <cstring>
#include <iostream>

curr_rate** filter(curr_rate* array[], int size, bool (*check)(curr_rate* element), int& result_size)
{
	curr_rate** result = new curr_rate * [size];
	result_size = 0;
	for (int i = 0; i < size; i++)
	{
		if (check(array[i]))
		{
			result[result_size++] = array[i];
		}
	}
	return result;
}

bool check_by_bank(curr_rate* element)
{
	return strcmp(element->bank, "�����������") == 0;
		
}

bool check_by_sell(curr_rate* element)
{
	return element->sell < 2.5;
}
